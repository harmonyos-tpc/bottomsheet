/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.flipboard.bottomsheet.sample.slice;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.IntentPickerSheetView;
import com.flipboard.bottomsheet.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.accessibility.ability.AccessibleAbility;
import ohos.accessibility.ability.SoftKeyBoardController;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;

public class PickerAbilitySlice extends AbilitySlice {
    private Button share_button;
    private TextField share_text;
    protected BottomSheetLayout bottomSheetLayout;

    @Override
    public void onStart(Intent intent)
    {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_picker);

        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_picker, null, false);

        bottomSheetLayout =(BottomSheetLayout) rootLayout.findComponentById(ResourceTable.Id_bottomsheet);
        share_text = (TextField) rootLayout.findComponentById(ResourceTable.Id_share_text);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.LINE);
        shapeElement.setStroke(2, new RgbColor(54, 54, 54));
        share_text.setBasement(shapeElement);

        share_button = (Button) rootLayout.findComponentById(ResourceTable.Id_share_button);
        share_button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                share_text.clearFocus();
                SoftKeyBoardController ime = new SoftKeyBoardController(AccessibleAbility.SHOW_MODE_AUTO, null);
                ime.setShowMode(AccessibleAbility.SHOW_MODE_HIDE);
                Intent shareIntent = new Intent();
                shareIntent.addFlags(Intent.FLAG_ABILITY_CONTINUATION);
                IntentPickerSheetView intentPickerSheet = new IntentPickerSheetView(PickerAbilitySlice.this, shareIntent,
                        "Share with...", new IntentPickerSheetView.OnIntentPickedListener() {
                    @Override
                    public void onIntentPicked(IntentPickerSheetView.ActivityInfo activityInfo) {
                    }
                });

                bottomSheetLayout.showWithSheetView(intentPickerSheet);
            }
        });

        super.setUIContent(rootLayout);
    }
    @Override
    public void onActive() {
        super.onActive();
    }
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
