/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.flipboard.bottomsheet.sample.slice;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

public class AbilityTest extends AbilitySlice {

    private Button _button;
    protected BottomSheetLayout bottomSheetLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_bottom_sheet_fragment);

        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_bottom_sheet_fragment, null, false);
        super.setUIContent(rootLayout);

        bottomSheetLayout =(BottomSheetLayout) rootLayout.findComponentById(ResourceTable.Id_plLayoutRoot);
        _button = (Button) rootLayout.findComponentById(ResourceTable.Id_button);
        _button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // Hide the keyboard
                bottomSheetLayout.showWithSheetView(LayoutScatter.getInstance(AbilityTest.this)
                        .parse(ResourceTable.Layout_ability_main_Frag, bottomSheetLayout, false));
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
