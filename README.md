## Bottomsheet library

## Introduction

BottomSheet is a openharmony component which presents a dismissible view from the bottom of the screen. BottomSheet can be a useful replacement for dialogs and menus but can hold any view so the use cases are endless. This repository includes the BottomSheet component itself but also includes a set of common view components presented in a bottom sheet. These are located in the commons module.

### Features

It covers below cases
1)Intentpicker.
2)Simple bottomsheet.


## Usage Instructions
Get started by wrapping your layout in a BottomSheetLayout:

	<com.flipboard.bottomsheet.BottomSheetLayout
		xmlns:ohos="http://schemas.huawei.com/res/ohos"
		ohos:id="$+id:plLayoutRoot"
		ohos:height="match_parent"
		ohos:width="match_parent">
		<StackLayout
			xmlns:ohos="http://schemas.huawei.com/res/ohos"
			ohos:height="match_parent"
			ohos:width="match_parent"
			ohos:orientation="vertical">

        <Button
            ohos:id="$+id:button"
            ohos:height="40vp"
            ohos:width="300vp"
            ohos:background_element="$graphic:background_ability_main"
            ohos:top_margin="20vp"
            ohos:left_margin="20vp"
            ohos:right_margin="20vp"
            ohos:text="Show"
            ohos:text_size="50"
            />
		</StackLayout>
	</com.flipboard.bottomsheet.BottomSheetLayout>


Back in your ability you would get a reference to the BottomSheetLayout like any other view.

	bottomSheetLayout =(BottomSheetLayout) rootLayout.findComponentById(ResourceTable.Id_plLayoutRoot);

Now all you need to do is show a view in the bottomSheet:

	bottomSheetLayout.showWithSheetView(LayoutScatter.getInstance(AbilityTest.this)
                        .parse(ResourceTable.Layout_ability_main_Frag, bottomSheetLayout, false));
						
You could also use one of the sheet views from the commons module.



## Installation instruction

```
Solution 1: local har package integration
Add the .har package to the lib folder.
Add the following code to the gradle of the entry:
implementation fileTree(dir: 'libs', include: ['.jar', '.har'])

Solution 2: Add following dependencies in your build.gradle:

allprojects {
    repositories {
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:bottomsheet:1.0.2'
implementation 'io.openharmony.tpc.thirdlib:bottomsheet_commons:1.0.1' //optional

```

