package com.flipboard.bottomsheet.commons;

import com.flipboard.bottomsheet.ViewTransformer;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.LayoutScatter;
import ohos.utils.PacMap;

public class BottomSheetFragment extends Fraction implements BottomSheetFragmentInterface {
    private BottomSheetFragmentDelegate delegate;

    public BottomSheetFragment() { }

    @Override
    public void show(FractionManager manager, int bottomSheetLayoutId) {
        getDelegate().show(manager, bottomSheetLayoutId);
    }

    @Override
    public int show(FractionScheduler transaction, int bottomSheetLayoutId) {
        return 0;
    }

    @Override
    protected void onStart(Intent intent) {
        getDelegate().onAttach(getContext());
        getDelegate().onCreate(null);
        getDelegate().getLayoutInflater();
        getDelegate().onStart();
        super.onStart(intent);
    }

    @Override
    protected void onActive() {

        super.onActive();
    }

    @Override
    public void onInactive() {
        getDelegate().dismiss();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }


    @Override
    public ViewTransformer getViewTransformer() {
        return null;
    }


    @Override
    public void onStop() {
        super.onStop();
        getDelegate().onDetach();
    }

    @Override
    protected void onComponentDetach() {
        super.onComponentDetach();
    }

    public LayoutScatter getLayoutInflater(PacMap savedInstanceState) {
        return getDelegate().getLayoutInflater(/*savedInstanceState, null*/);
   }

    public void onDestroyView() {
       getDelegate().onDestroyView();
    }

    private BottomSheetFragmentDelegate getDelegate() {
        if (delegate == null) {
            delegate = BottomSheetFragmentDelegate.create(this);
        }
        return delegate;
    }
}
